<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\fileSystem\folder
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\fileSystem\folder\FolderException;
	use nuclio\plugin\fileSystem\reader\FileReader;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\fileSystem\driver\common\CommonInterface;

	enum RULE:	int
	{
		RECURSIVE		=	1;
		REMOVE_EMPTY	=	2;
	}
	/**
	 * Manage Folder functionalities.
	 *
	 * This class provides the ability to manage CURD over Directories with 
	 * 
	 */
	<<factory>>
	class Folder extends Plugin implements Iterator<mixed>
	{

		
		private CommonInterface		$driver;
		private string				$driverName;
		private string				$location;
		private Vector<string>		$list;

		private int					$cursor;
		private mixed 				$current = null;

		public static function getInstance(/* HH_FIXME[4033] */...$args):Folder
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(string $driver, string $path)
		{
			parent::__construct();
			
			$this->driverName = $driver;
			$tmpDriver=ProviderManager::request($driver);
			if($tmpDriver instanceof CommonInterface)
			{
				$this->driver = $tmpDriver;
			}
			else
			{
				throw new FolderException(sprintf('No driver found for "%s".',$driver));
			}
			if(!$this->getDriver()->isDirectory($path))
			{
				throw new FolderException(sprintf('"%s" Is not a folder',$path));
			}
			$this->location =	$path;
			$this->cursor 	=	0;
			$this->list 	=	$this->getDriver()->listAllFilesAndFolders($this->location);
		}

		/**
		 * Get the count of the items inside the folder
		 *
		 * @access public
		 * 
		 * @return     int  number of items in the folder
		 */
		public function count():int
		{
			return count($this->list);
		}
		/**
		 * Provide informatoin regarding the current forlder
		 * 
		 * @access public
		 *
		 * @return     mixed  current folder information
		 */
		public function current():mixed
		{
			return $this->current;
		}
		/**
		 * The order of the current folder in the list
		 * 
		 * @access public
		 *
		 * @return     int  current folder order
		 */
		public function key():int
		{
			return $this->cursor;
		}
		/**
		 * Move to the next folder in the list
		 * 
		 * @access public
		 * 
		 */
		public function next():void
		{
			++$this->cursor;
			$this->setCurrent();
		}
		/**
		 * { function_description }
		 */
		public function rewind():void
		{
			$this->cursor = 0;
			$this->setCurrent();
		}
		/**
		 * check if the cursor value is in the list of the files in the current folder
		 * 
		 * @access public
		 *
		 * @return     bool  true if the current folder is in the list
		 */
		public function valid():bool
		{
			return ($this->cursor < $this->count());
		}

		/**
		 * Get the driver Name.
		 * 
		 * @access private
		 *
		 * @return     string  Driver name.
		 */
		private function getDriver():CommonInterface
		{
			return $this->driver;
		}
		/**
		 * Get the driver name.
		 *
		 * @return     <type>  Driver name.
		 */
		public function getDriverName():string
		{
			return $this->driverName;
		}
		public function getLocation():string
		{
			return $this->location;
		}
		/**
		 * Get the folder's path.
		 * 
		 * @access public
		 *
		 * @return     string  Path.
		 */
		public function getPath():string
		{
			return $this->list[$this->key()];	
		}
		/**
		 * Get the current folder full path.
		 * 
		 * @access public
		 *
		 * @return     string  Full path.
		 */
		public function getFullPath():string
		{
			return $this->getLocation() . _DS_ . $this->list[$this->key()];
		}

		/**
		 * Set the change the current folder
		 * 
		 * @access private
		 * 
		 */
		private function setCurrent():void
		{
			//I don't like this
			if(!$this->valid())
			{
				return;
			}
			if($this->isEmpty())
			{
				$this->current = null;
				return;
			}
			$currentKey = $this->getLocation() . _DS_ . $this->list[$this->cursor];
			if($this->list[$this->cursor] == '.')
			{
				$currentKey = $this->location;
			}
			if($this->list[$this->cursor] == '..')
			{
				$exploded = explode(_DS_, $this->location);
				$currentKey = implode(_DS_,array_slice($exploded, 0, count($exploded)-2));;
			}

			if($this->getDriver()->isFile($currentKey))
			{
				$this->current = FileReader::getInstance($this->getDriverName(), $currentKey);
			}
			else if($this->getDriver()->isDirectory($currentKey))
			{
				$this->current = self::getInstance($this->getDriverName(), $currentKey);
			}
			else
			{
				throw new FolderException("Neither file nor folder. Something's wrong.");	
			}
		}

		/**
		 * Check weather the current element is '.' or '..'
		 * 
		 * @access public
		 * 
		 * @return boolean True/False if current element is '.' or '..' or not.
		 */
		public function isDot():bool
		{
			return ($this->list[$this->key()]=='.' || $this->list[$this->key()]=='..');
		}

		/**
		 * Check if the file exist or not.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return bool       	True/False for file existance.
		 */
		public function exists():bool
		{
			return $this->getDriver()->exists($this->getLocation());
		}

		/**
		 * Check if the path given is a file.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path to the file.
		 * @return boolean       True/False for file or not.
		 */
		public function isFile():bool
		{
			return $this->getDriver()->isFile($this->getLocation());
		}

		/**
		 * Check whether the param given is path or not.
		 * 
		 * @access public
		 * 
		 * @param  string  $directory Path to a directory
		 * @return boolean            True/False for the path or not.
		 */
		public function isDirectory():bool
		{
			return $this->getDriver()->isDirectory($this->getLocation());
		}

		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isReadable():bool
		{
			return $this->getDriver()->isReadable($this->getLocation());
		}

		/**
		 * Check if the given path is writeable.
		 * 
		 * @access public
		 * 
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable():bool
		{
			return $this->getDriver()->isWritable($this->getLocation());
		}

		/**
		 * Determine if empty.
		 * 
		 * @access public
		 *
		 * @return     boolean  True if empty, False otherwise.
		 */
		public function isEmpty():bool
		{
			return $this->getDriver()->isEmpty($this->getLocation());
		}

		/**
		 * Delete file.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file to be deleted.
		 * @return mixed       	True if delete success, error message if failed.
		 */
		public function delete(int $rules):mixed
		{
			if($rules & (int)RULE::RECURSIVE)
			{
				foreach($this->list as $item)
				{
					$this->getDriver()->delete($this->getLocation() . _DS_ . $item, $rules);
				}
			}
			if($rules & (int)RULE::REMOVE_EMPTY)
			{
				foreach($this->list as $item)
				{
					$path = $this->getLocation() . _DS_ . $item;
					if($this->getDriver()->isDirectory($path) && $this->getDriver()->isEmpty($path))
					{
						$this->getDriver()->delete($path, $rules);
					}
				}	
			}
		}

		/**
		 * Move file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be moved.
		 * @param  string $target 	Where to move.
		 * @return bool         	True/False for the move process.
		 */
		public function move(string $target, int $rules):bool
		{
			return $this->getDriver()->move($this->getLocation(), $target, $rules);		
		}

		/**
		 * Copy file to another place.
		 * 
		 * @access public
		 * 
		 * @param  string $path   	File to be copied.
		 * @param  string $target 	Where to copied.
		 * @return bool         	True/False for the copied process.
		 */
		 
		public function copy(string $target):bool
		{
			return $this->getDriver()->copy($this->getLocation(), $target);
		}

		/**
		 * Creates a new directory at the given path
		 * 
		 * @access public
		 * 
		 * @param string $path 		Path to be created.
		 * @return  bool  			Treu/false on success/failure
		 */
		public function create(string $name):bool
		{
			return $this->getDriver()->create($this->getLocation() . _DS_ . $name);
		}

		/**
		 * Retruns the contents of the folder at the passed path
		 * 
		 * @access public
		 * 
		 * @param  string $path 
		 * @return string       contents of the given path
		 */
		public function listAllFilesAndFolders():Vector<string>
		{
			return $this->list;
		}

		/**
		 * Returns a list of all the files/folders matching the given regex string inside the given path.
		 * 
		 * @access public
		 * 
		 * @param  string $path  Path to perform the search into.
		 * @param  string $regex regex expression to be matched
		 * @return string List of all the search results
		 */
		public function find(string $regex):Vector<string>
		{
			return $this->getDriver()->find($this->getLocation(), $regex);
		}

		/**
		 * Get the dir name.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName():?string
		{
			return end(explode(_DS_, $this->getLocation()));
		}

		/**
		 * Here for compatiblity fith file handlers. returns empty string.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt():?string
		{
			return '';
		}

		/**
		 * Here for compatiblity fith file handlers. returns Dir
		 * 
		 * @access publi
		 * 
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType():?string
		{
			return 'Dir';
		}

		/**
		 * Get the file size.
		 * 
		 * @access public
		 * 
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize():?int
		{
			return $this->getDriver()->getSize($this->getLocation());
		}
	}
}
